//
//  CustomTableViewCell.swift
//  MyPlaces
//
//  Created by MAX KD on 2022/9/12.
//

import UIKit

class CustomTableViewCell: UITableViewCell {


    @IBOutlet weak var imageOfPlace: UIImageView!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var LocationLabel: UILabel!
    @IBOutlet weak var TypeLabel: UILabel!
}
